package ie.tcd.scss.colorregistry.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ColorControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    // Test to reset the color map
    @Test
    public void testReset() {
        ResponseEntity<String> response = restTemplate.getForEntity("/reset", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Registry has been reset.", response.getBody());  // Assuming reset returns this message
    }

    // Test to add a valid color
    @Test
    public void testAddColor() {
        Map<String, String> color = Map.of("name", "Red", "hex", "#FF0000");
        ResponseEntity<String> response = restTemplate.postForEntity("/colors", color, String.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Color added successfully.", response.getBody());
    }

    // Test to add a color with a missing hex value (should be rejected with 403)
    @Test
    public void testAddColorWithoutHex() {
        Map<String, String> color = Map.of("name", "Blue");
        ResponseEntity<String> response = restTemplate.postForEntity("/colors", color, String.class);
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals("Hex value is required.", response.getBody());
    }

    // Test to get all colors and ensure alphabetical order
    @Test
    public void testGetAllColors() {
        // Reset the registry and add multiple colors
        restTemplate.getForEntity("/reset", String.class);
        restTemplate.postForEntity("/colors", Map.of("name", "Green", "hex", "#00FF00"), String.class);
        restTemplate.postForEntity("/colors", Map.of("name", "Blue", "hex", "#0000FF"), String.class);
        restTemplate.postForEntity("/colors", Map.of("name", "Red", "hex", "#FF0000"), String.class);

        // Retrieve all colors
        ResponseEntity<String> response = restTemplate.getForEntity("/colors", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String expectedResponse = "{ \"Blue\":\"#0000FF\", \"Green\":\"#00FF00\", \"Red\":\"#FF0000\" }";
        assertEquals(expectedResponse, response.getBody());
    }

    // Test to get all colors when registry is empty
    @Test
    public void testGetAllColorsWhenEmpty() {
        // Reset the registry to ensure it's empty
        restTemplate.getForEntity("/reset", String.class);

        // Retrieve all colors from an empty registry
        ResponseEntity<String> response = restTemplate.getForEntity("/colors", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Registry is empty.", response.getBody());
    }

    // Test to retrieve an existing color
    @Test
    public void testGetColor() {
        restTemplate.postForEntity("/colors", Map.of("name", "Blue", "hex", "#0000FF"), String.class);
        ResponseEntity<String> response = restTemplate.getForEntity("/colors/Blue", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("#0000FF", response.getBody());
    }

}
