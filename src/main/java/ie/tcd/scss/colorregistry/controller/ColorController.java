package ie.tcd.scss.colorregistry.controller;

import ie.tcd.scss.colorregistry.dto.ColorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;



@RestController
@RequestMapping
public class ColorController {
    private final Map<String, String> colorRegistry = new HashMap<>();

    @GetMapping("/reset")
    public ResponseEntity<String> resetReg() {
        colorRegistry.clear();
        return new ResponseEntity<>("Registry has been reset.",HttpStatus.OK);
    }

    @PostMapping("/colors")
    public ResponseEntity<String> colorRegHandler(@RequestBody ColorDto colorDto) {
        if(colorDto.getName()==null){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        if(colorDto.getHex()==null){
            return new ResponseEntity<>("Hex value is required.",HttpStatus.FORBIDDEN);
        }
        
        String name = colorDto.getName().trim();
        String hex = colorDto.getHex().trim();

        if(colorRegistry.containsKey(name)){
            colorRegistry.put(name,hex);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else{
            colorRegistry.put(name,hex);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
    }
        @GetMapping("/colors/{name}")
        public ResponseEntity<String> getColorbyName(@PathVariable String name){
            if(colorRegistry.containsKey(name)){
                return new ResponseEntity<>("" + colorRegistry.get(name) + "",HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>("Color not found.", HttpStatus.NOT_FOUND);
            }

        }
       
        @GetMapping("/colors")
        public ResponseEntity<?> getAllColors(){
            if(colorRegistry.isEmpty()){
                return new ResponseEntity<>("Registry is empty.",HttpStatus.OK);
            }
            else{
                Map<String, String> sorted = new TreeMap<>(colorRegistry);
                return new ResponseEntity<>(sorted,HttpStatus.OK);

            
        }
    }
}
